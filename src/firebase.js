import firebase from "firebase/app";
import 'firebase/firestore'

const firebaseconfig = firebase.initializeApp({
    apiKey: "AIzaSyBMtmAzvDtOEmC7r49vK97JAWchHi3MC8I",
    authDomain: "todoist-79ed4.firebaseapp.com",
    databaseURL: "https://todoist-79ed4-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "todoist-79ed4",
    storageBucket: "todoist-79ed4.appspot.com",
    messagingSenderId: "180919566218",
    appId: "1:180919566218:web:9999097a330e9b8462d273",
    measurementId: "G-BB3920KZ87"
})

export { firebaseconfig as firebase };